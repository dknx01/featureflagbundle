<?php
/**
 *
 * @author dknx01 <e.witthauer@gmail.com>
 * @since 09.07.16 20:04
 * @package
 *
 */

namespace Dknx01\FeatureFlagBundle\Exception;

use Exception;

class NoSuchConfigurationKeyException extends \Exception
{
    /**
     * @inheritdoc
     */
    public function __construct($message, $code = 0, Exception $previous = null)
    {
        $message = 'Key "' . $message . '" has not been defined in your parameters.yml file, but is needed for ' .
            'FeatureFlag to be working correctly.';
        parent::__construct($message, $code, $previous);
    }
}