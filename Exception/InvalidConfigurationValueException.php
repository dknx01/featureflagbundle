<?php
/**
 *
 * @author dknx01 <e.witthauer@gmail.com>
 * @since 10.07.16 15:24
 * @package
 *
 */

namespace Dknx01\FeatureFlagBundle\Exception;

class InvalidConfigurationValueException extends \Exception
{
}