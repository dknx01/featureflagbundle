<?php
/**
 *
 * @author dknx01 <e.witthauer@gmail.com>
 * @since 09.07.16 20:25
 * @package
 *
 */

namespace Dknx01\FeatureFlagBundle\Exception;

class FlagNotFoundException extends \Exception
{
}