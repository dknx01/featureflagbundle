<?php
/**
 *
 * @author dknx01 <e.witthauer@gmail.com>
 * @since 09.07.16 20:11
 * @package
 *
 */

namespace Dknx01\FeatureFlagBundle\Handler;

use Dknx01\FeatureFlagBundle\Entity\FlagCollection;
use Dknx01\FeatureFlagBundle\Exception\FlagNotFoundException;
use Dknx01\FeatureFlagBundle\Exception\InvalidConfigurationValueException;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ConfigFileHandler implements HandlerInterface
{
    const FEATURE_FLAGS_PARAMETER_NAME = 'dknx01_feature_flags';

    /**
     * @var array
     */
    private $flags;

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * ConfigFileHandler constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->setFlags($container->getParameter(self::FEATURE_FLAGS_PARAMETER_NAME)['flags']);
    }

    /**
     * @inheritdoc
     * @throws FlagNotFoundException
     */
    public function isActive($flag)
    {
        if (!$this->flagExists($flag)) {
            throw new FlagNotFoundException('Flag ' . $flag . ' is not defined.');
        }
        $flagValue = $this->flags[$flag];

        return $this->checkIp($flagValue)
            && $this->checkEnvironment($flagValue)
            && $this->checkSapi($flagValue)
            && $this->checkDate($flagValue)
            && $this->checkTime($flagValue);
    }

    /**
     * @inheritdoc
     */
    public function flagExists($flag)
    {
        return array_key_exists($flag, $this->flags);
    }

    /**
     * @inheritdoc
     */
    public function getAllFlags()
    {
        return new FlagCollection($this->flags);
    }

    /**
     * @param array $flags
     *
     * @return ConfigFileHandler
     */
    public function setFlags($flags)
    {
        $this->flags = $flags;
        return $this;
    }

    /**
     * @param array $flagValue
     * @return bool
     */
    private function checkIp(array $flagValue)
    {
        return isset($flagValue['ip'])
            ? in_array($this->container->get('request')->getClientIp(), (array)$flagValue['ip'])
            : true;
    }

    private function checkEnvironment(array $flagValue)
    {

        return isset($flagValue['environment'])
            ? in_array($this->container->getParameter('kernel.environment'), (array)$flagValue['environment'])
            : true;
    }

    /**
     * @param array $flagValue
     * @return true
     */
    private function checkSapi(array $flagValue)
    {
        if (isset($flagValue['sapi'])) {
            if (isset($flagValue['sapi_negation']) && $flagValue['sapi_negation'] === true) {
                return !in_array(php_sapi_name(), (array)$flagValue['sapi']);
            } else {
                return in_array(php_sapi_name(), (array)$flagValue['sapi']);
            }
        } else {
            return true;
        }
    }

    /**
     * @param array $flagValue
     * @return bool
     */
    private function checkDate(array $flagValue)
    {
        if (isset($flagValue['date'])) {
            $now = new \DateTime();
            $start = null;
            $startCheck = true;
            $end = null;
            $endCheck = true;
            $pattern = '^\d{4}-\d{2}-\d{2}\ \d{2}:\d{2}:\d{2}$';
            if (isset($flagValue['date']['start'])) {
                $start = $this->getValueFromRegex($flagValue['date']['start'], $pattern);
               $startCheck = $now->getTimestamp() >= \DateTime::createFromFormat('Y-m-d H:i:s', $start)->getTimestamp();
            }if (isset($flagValue['date']['end'])) {
                $end = $this->getValueFromRegex($flagValue['date']['end'], $pattern);
                $endCheck = $now->getTimestamp() <= \DateTime::createFromFormat('Y-m-d H:i:s', $end)->getTimestamp();
            }
            return $startCheck && $endCheck;
        } else {
            return true;
        }
    }

    /**
     * @param array $flagValue
     * @return bool
     * @throws InvalidConfigurationValueException
     */
    private function checkTime(array $flagValue)
    {
        if (isset($flagValue['time'])) {
            $now = new \DateTime();
            $start = clone $now;
            $startCheck = true;
            $end = clone $now;
            $endCheck = true;
            $pattern = '^(?P<hour>\d{2}):(?P<minute>\d{2}):(?P<second>\d{2})$';
            if (isset($flagValue['time']['start'])) {
                $startValues = $this->getTimeValues($flagValue['time']['start'], $pattern);
                $start->setTime($startValues['hour'], $startValues['minute'], $startValues['second']);
                $startCheck = $now >= $start;
            }if (isset($flagValue['time']['end'])) {
                $endValues = $this->getTimeValues($flagValue['time']['end'], $pattern);
                $end->setTime($endValues['hour'], $endValues['minute'], $endValues['second']);
                $endCheck = $now <= $end;
            }
            return $startCheck && $endCheck;
        } else {
            return true;
        }
    }

    /**
     * @param string$data
     * @param string $pattern
     * @return string
     * @throws InvalidConfigurationValueException
     */
    private function getValueFromRegex($data, $pattern)
    {
        if (!preg_match('/' . $pattern . '/', $data)) {
            throw new InvalidConfigurationValueException('The value ' . $data . ' does not match pattern ' . $pattern);
        }
        return $data;
    }

    /**
     * @param string $data
     * @param string $pattern
     * @return array
     * @throws InvalidConfigurationValueException
     */
    private function getTimeValues($data, $pattern)
    {
        if (!preg_match('/' . $pattern . '/', $data, $matches)) {
            throw new InvalidConfigurationValueException('The value ' . $data . ' does not match pattern ' . $pattern);
        }
        return $matches;
    }
}