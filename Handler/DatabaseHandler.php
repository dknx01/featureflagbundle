<?php
/**
 *
 * @author dknx01 <e.witthauer@gmail.com>
 * @since 10.07.16 21:03
 * @package
 *
 */

namespace Dknx01\FeatureFlagBundle\Handler;

use Dknx01\FeatureFlagBundle\Entity\FeatureFlag;
use Dknx01\FeatureFlagBundle\Entity\FlagCollection;
use Dknx01\FeatureFlagBundle\Exception\FlagNotFoundException;
use Dknx01\FeatureFlagBundle\Exception\InvalidConfigurationValueException;
use Dknx01\FeatureFlagBundle\Service\DatabaseUtils;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @codeCoverageIgnore
 */
class DatabaseHandler extends DatabaseUtils implements HandlerInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @inheritdoc
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * @inheritdoc
     * @throws FlagNotFoundException
     * @throws InvalidConfigurationValueException
     */
    public function isActive($flag)
    {
        $featureFlag = $this->container->get('doctrine.orm.entity_manager')->getRepository(FeatureFlag::class)
            ->findOneBy(array('name' => $flag));
        if ($featureFlag instanceof FeatureFlag) {
            return $this->checkIp($featureFlag)
                && $this->checkEnvironment($featureFlag)
                && $this->checkSapi($featureFlag)
                && $this->checkDate($featureFlag)
                && $this->checkTime($featureFlag);
        } else {
            throw new FlagNotFoundException('Flag ' . $flag . ' is not defined.');
        }
    }

    /**
     * @inheritdoc
     */
    public function flagExists($flag)
    {
        $result = $this->container->get('doctrine.orm.entity_manager')->getRepository(FeatureFlag::class)
            ->findOneBy(array('name' => $flag));
        return $result !== null;
    }

    /**
     * @inheritdoc
     */
    public function getAllFlags()
    {
        $this->createTableIfNotExists();
        $entries = $this->container->get('doctrine.orm.entity_manager')->getRepository(FeatureFlag::class)
            ->findAll();
        $collection = new FlagCollection($entries);
        return $collection;
    }

    /**
     * @param FeatureFlag $featureFlag
     * @return bool
     */
    private function checkIp(FeatureFlag $featureFlag)
    {
        return count($featureFlag->getIp()) > 0
            ? in_array($this->container->get('request')->getClientIp(), $featureFlag->getIp())
            : true;
    }

    /**
     * @param FeatureFlag $featureFlag
     * @return bool
     */
    private function checkEnvironment(FeatureFlag $featureFlag)
    {
        return count($featureFlag->getEnvironment()) > 0
            ? in_array($this->container->getParameter('kernel.environment'), $featureFlag->getEnvironment())
            : true;
    }

    /**
     * @param FeatureFlag $featureFlag
     * @return bool
     */
    private function checkSapi(FeatureFlag $featureFlag)
    {
        if (count($featureFlag->getSapi()) > 0) {
            if ($featureFlag->isSapiNegation()) {
                return !in_array(php_sapi_name(), $featureFlag->getSapi());
            } else {
                return in_array(php_sapi_name(), $featureFlag->getSapi());
            }
        } else {
            return true;
        }
    }

    /**
     * @param FeatureFlag $featureFlag
     * @return bool
     */
    private function checkDate(FeatureFlag $featureFlag)
    {
        $now = new \DateTime();

        $startCheck = true;
        if ($featureFlag->getDateStart() instanceof \DateTime) {
            $startCheck = $now >= $featureFlag->getDateStart();
        }
        $endCheck = true;
        if ($featureFlag->getDateEnd() instanceof \DateTime) {
            $endCheck = $now <= $featureFlag->getDateEnd();
        }
        return $startCheck && $endCheck;
    }

    /**
     * @param FeatureFlag $featureFlag
     * @return bool
     * @throws InvalidConfigurationValueException
     */
    private function checkTime(FeatureFlag $featureFlag)
    {
        $now = new \DateTime();

        $pattern = '^(?P<hour>\d{2}):(?P<minute>\d{2}):(?P<second>\d{2})$';

        $startCheck = true;
        if ($featureFlag->getTimeStart() !== null) {
            $startTime = clone $now;
            $startValues = $this->getTimeValues($featureFlag->getTimeStart(), $pattern);
            $startTime->setTime($startValues['hour'], $startValues['minute'], $startValues['second']);
            $startCheck = $now >= $startTime;
        }
        $endCheck = true;
        if ($featureFlag->getTimeStart() !== null) {
            $endTime = clone $now;
            $endValues = $this->getTimeValues($featureFlag->getTimeEnd(), $pattern);
            $endTime->setTime($endValues['hour'], $endValues['minute'], $endValues['second']);
            $endCheck = $now <= $endTime;
        }
        return $startCheck && $endCheck;
    }

    /**
     * @param string $data
     * @param string $pattern
     * @return array
     * @throws InvalidConfigurationValueException
     */
    private function getTimeValues($data, $pattern)
    {
        if (!preg_match('/' . $pattern . '/', $data, $matches)) {
            throw new InvalidConfigurationValueException('The value ' . $data . ' does not match pattern ' . $pattern);
        }
        return $matches;
    }
}