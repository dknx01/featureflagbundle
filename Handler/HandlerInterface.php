<?php
/**
 *
 * @author dknx01 <e.witthauer@gmail.com>
 * @since 09.07.16 19:57
 * @package
 *
 */

namespace Dknx01\FeatureFlagBundle\Handler;

use Dknx01\FeatureFlagBundle\Entity\FlagCollection;

interface HandlerInterface
{
    /**
     * @param string$flag
     * @return bool
     */
    public function isActive($flag);

    /**
     * @param string $flag
     * @return bool
     */
    public function flagExists($flag);

    /**
     * @return FlagCollection
     */
    public function getAllFlags();
}