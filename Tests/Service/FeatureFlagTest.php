<?php
/**
 *
 * @author dknx01 <e.witthauer@gmail.com>
 * @since 10.08.16 20:04
 * @package
 *
 */

namespace Service;

use Dknx01\FeatureFlagBundle\Entity\FlagCollection;
use Dknx01\FeatureFlagBundle\Handler\ConfigFileHandler;
use Dknx01\FeatureFlagBundle\Service\FeatureFlag;
use Dknx01\FeatureFlagBundle\Tests\Stubs\StubFeatureService;
use Dknx01\FeatureFlagBundle\Tests\Stubs\StubHandlerWithInterface;
use Dknx01\FeatureFlagBundle\Tests\Stubs\StubHandlerWithoutInterface;
use Monolog\Logger;
use Symfony\Component\DependencyInjection\Container;

class FeatureFlagTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage Dknx01\FeatureFlagBundle\Tests\Stubs\StubHandlerWithoutInterface does not implement
     * FeatureFlagBundle\Handler\HandlerInterface.
     */
    public function testWithInvalidHandlerClass()
    {
        $container = new Container();
        $container->set('logger', new Logger('foo'));

        $featureFlagService = new FeatureFlag();
        $featureFlagService->setContainer($container);

        $config = array(
            'handler' => StubHandlerWithoutInterface::class
        );

        $featureFlagService->initialize($config);
    }

    public function testWithHandlerClass()
    {
        $container = new Container();
        $container->set('logger', new Logger('foo'));

        $featureFlagService = new FeatureFlag();
        $featureFlagService->setContainer($container);

        $config = array(
            'handler' => StubHandlerWithInterface::class
        );

        $featureFlagService->initialize($config);

        self::assertEquals(StubHandlerWithInterface::class, $featureFlagService->getHandlerClass());
    }

    /**
     * @expectedException \Dknx01\FeatureFlagBundle\Exception\NoSuchConfigurationKeyException
     * @expectedExceptionMessage Key "handler" has not been defined in your parameters.yml file, but is needed for
     * FeatureFlag to be working correctly.
     */
    public function testWithInvalidConfig()
    {
        $container = new Container();
        $container->set('logger', new Logger('foo'));

        $featureFlagService = new FeatureFlag();
        $featureFlagService->setContainer($container);

        $config = array();

        $featureFlagService->initialize($config);
    }

    /**
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage foo.bar was not found.
     */
    public function testWithInvalidService()
    {
        $container = new Container();
        $container->set('logger', new Logger('foo'));

        $featureFlagService = new FeatureFlag();
        $featureFlagService->setContainer($container);

        $config = array(
            'handler' => 'foo.bar'
        );

        $featureFlagService->initialize($config);
    }

    public function testWithService()
    {
        $container = new Container();
        $container->set('logger', new Logger('foo'));
        $container->set('feature_flag.handler', new StubFeatureService());

        $featureFlagService = new FeatureFlag();
        $featureFlagService->setContainer($container);

        $config = array(
            'handler' => 'feature_flag.handler'
        );

        $featureFlagService->initialize($config);
        self::assertEquals(StubFeatureService::class, $featureFlagService->getHandlerClass());
    }

    public function testWithDefaultService()
    {
        $container = new Container();
        $container->set('logger', new Logger('foo'));
        $container->setParameter('dknx01_feature_flags', array('flags' => array()));
        $container->set('feature_flag.handler.config_file', new ConfigFileHandler($container));

        $featureFlagService = new FeatureFlag();
        $featureFlagService->setContainer($container);

        $config = array(
            'handler' => null
        );

        $featureFlagService->initialize($config);
        self::assertEquals(ConfigFileHandler::class, $featureFlagService->getHandlerClass());
    }

    public function testWithActiveFlag()
    {
        $container = new Container();
        $container->set('logger', new Logger('foo'));
        $container->setParameter('dknx01_feature_flags', array('flags' => array('foo' => array())));
        $container->set('feature_flag.handler.config_file', new ConfigFileHandler($container));

        $featureFlagService = new FeatureFlag();
        $featureFlagService->setContainer($container);

        $config = array(
            'handler' => null
        );

        $featureFlagService->initialize($config);
        self::assertTrue($featureFlagService->isActive('foo'));
    }

    public function testGetAllFlags()
    {
        $container = new Container();
        $container->set('logger', new Logger('foo'));
        $container->setParameter('dknx01_feature_flags', array('flags' => array('foo' => array())));
        $container->set('feature_flag.handler.config_file', new ConfigFileHandler($container));

        $featureFlagService = new FeatureFlag();
        $featureFlagService->setContainer($container);

        $config = array(
            'handler' => null
        );

        $featureFlagService->initialize($config);
        $flags = new FlagCollection(array('foo' => array()));
        self::assertEquals($flags, $featureFlagService->getAllFlags());
    }
}
