<?php
/**
 *
 * @author dknx01 <e.witthauer@gmail.com>
 * @since 07.08.16 21:13
 * @package
 *
 */

namespace Tests\Twig\Extension;

use Dknx01\FeatureFlagBundle\Entity\FeatureFlag;
use Dknx01\FeatureFlagBundle\Handler\ConfigFileHandler;
use Dknx01\FeatureFlagBundle\Twig\Extension\InstanceOfExtension;

class InstanceOfExtensionTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var InstanceOfExtension
     */
    private $twigExtension;

    /**
     * @inheritdoc
     */
    protected function setUp()
    {
        $this->twigExtension = new InstanceOfExtension();
    }

    public function testGetFilters()
    {
        $actual = $this->twigExtension->getFilters();

        self::assertInternalType('array', $actual);
        self::assertInstanceOf('\Twig_SimpleFilter', $actual[0]);
    }

    public function testGetName()
    {
        self::assertEquals('instance_of', $this->twigExtension->getName());
    }

    public function provideClassInstances()
    {
        return array(
            array(array(), 'array'),
            array(new \DateTime(), 'DateTime'),
            array(new FeatureFlag(), '\Dknx01\FeatureFlagBundle\Entity\FeatureFlag')
        );
    }

    /**
     * @dataProvider provideClassInstances
     * @param mixed $object
     * @param string $className
     */
    public function testInstanceOfClass($object, $className)
    {
        self::assertTrue($this->twigExtension->instanceOfClass($object, $className));
    }
}
