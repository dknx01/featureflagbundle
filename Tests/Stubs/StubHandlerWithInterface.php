<?php
/**
 *
 * @author dknx01 <e.witthauer@gmail.com>
 * @since 14.08.16 15:53
 * @package
 *
 */

namespace Dknx01\FeatureFlagBundle\Tests\Stubs;

use Dknx01\FeatureFlagBundle\Handler\HandlerInterface;

class StubHandlerWithInterface implements HandlerInterface
{
    public function isActive($flag)
    {
        // TODO: Implement isActive() method.
    }

    public function flagExists($flag)
    {
        // TODO: Implement flagExists() method.
    }

    public function getAllFlags()
    {
        // TODO: Implement getAllFlags() method.
    }
}