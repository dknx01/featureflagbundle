<?php
/**
 *
 * @author dknx01 <e.witthauer@gmail.com>
 * @since 08.08.16 20:16
 * @package
 *
 */

namespace Handler;

use Dknx01\FeatureFlagBundle\Entity\FlagCollection;
use Dknx01\FeatureFlagBundle\Handler\ConfigFileHandler;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpFoundation\Request;

class ConfigFileHandlerTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var ConfigFileHandler
     */
    private $configFileHandler;

    /**
     * @var array
     */
    private $flags = array(
        'foo' => array()
    );

    protected function setUp()
    {
        $container = new Container();

        $container->setParameter(ConfigFileHandler::FEATURE_FLAGS_PARAMETER_NAME, array('flags' => $this->flags));
        $this->configFileHandler = new ConfigFileHandler($container);
    }

    public function testFlagExists()
    {
        self::assertTrue($this->configFileHandler->flagExists('foo'));
    }

    public function testGetAllFlags()
    {
        $result = $this->configFileHandler->getAllFlags();
        self::assertInstanceOf(FlagCollection::class, $result);
        self::assertEquals(1, $result->count());
        self::assertEquals($this->flags, $result->toArray());
    }

    public function testSetFlags()
    {
        $this->configFileHandler->setFlags($this->flags);
        self::assertEquals($this->flags, $this->configFileHandler->getAllFlags()->toArray());
    }

    /**
     * @expectedException \Dknx01\FeatureFlagBundle\Exception\FlagNotFoundException
     */
    public function testIsActiveWithInvalidFlags()
    {
        $this->configFileHandler->isActive('bar');
    }

    public function testisActiveValid()
    {
        self::assertTrue($this->configFileHandler->isActive('foo'));
    }

    /**
     * @return array
     */
    public function provideIpAddresses()
    {
        return array(
            array(true, '127.0.0.0'),
            array(true, array('127.0.0.0', '127.0.0.1')),
            array(false, array('543.321.0.0'))
        );
    }

    /**
     * @dataProvider provideIpAddresses
     * @param boolean $expected
     * @param string $flagValue
     */
    public function testCheckIp($expected, $flagValue)
    {
        $container = new Container();
        $container->setParameter(ConfigFileHandler::FEATURE_FLAGS_PARAMETER_NAME, array('flags' => $this->flags));

        $request = new Request();
        $request->server->set('REMOTE_ADDR', '127.0.0.0');
        $container->set('request', $request);

        $configFileHandler = new ConfigFileHandler($container);
        $classRefl = new \ReflectionClass($configFileHandler);
        $methodRefl = $classRefl->getMethod('checkIp');
        $methodRefl->setAccessible(true);

        self::assertEquals($expected, $methodRefl->invoke($configFileHandler, array('ip' => $flagValue)));
    }

    /**
     * @return array
     */
    public function provideEnvironment()
    {
        return array(
            array(true, 'test'),
            array(true, array('bar', 'test')),
            array(false, array('foo'))
        );
    }

    /**
     * @dataProvider provideEnvironment
     * @param boolean $expected
     * @param string $flagValue
     */
    public function testCheckEnvironment($expected, $flagValue)
    {
        $container = new Container();
        $container->setParameter(ConfigFileHandler::FEATURE_FLAGS_PARAMETER_NAME, array('flags' => $this->flags));
        $container->setParameter('kernel.environment', 'test');

        $configFileHandler = new ConfigFileHandler($container);
        $classRefl = new \ReflectionClass($configFileHandler);
        $methodRefl = $classRefl->getMethod('checkEnvironment');
        $methodRefl->setAccessible(true);

        self::assertEquals($expected, $methodRefl->invoke($configFileHandler, array('environment' => $flagValue)));
    }

    /**
     * @return array
     */
    public function provideSapi()
    {
        return array(
            array(true, array('sapi' => 'cli')),
            array(true, array('sapi' => array('cli', 'test'))),
            array(false, array('sapi' => array('web'))),
            array(false, array('sapi' => array('web'), 'sapi_negation' => false)),
            array(true, array('sapi' => array('web'), 'sapi_negation' => true))
        );
    }

    /**
     * @dataProvider provideSapi
     * @param boolean $expected
     * @param string $flagValue
     */
    public function testCheckSapi($expected, $flagValue)
    {
        $container = new Container();
        $container->setParameter(ConfigFileHandler::FEATURE_FLAGS_PARAMETER_NAME, array('flags' => $this->flags));

        $configFileHandler = new ConfigFileHandler($container);
        $classRefl = new \ReflectionClass($configFileHandler);
        $methodRefl = $classRefl->getMethod('checkSapi');
        $methodRefl->setAccessible(true);

        self::assertEquals($expected, $methodRefl->invoke($configFileHandler, $flagValue));
    }

    /**
     * @return array
     */
    public function provideDate()
    {
        return array(
            array(true, array('date' => array('start' => '2016-01-01 12:13:00'))),
            array(false, array('date' => array('start' => '2216-01-01 12:13:00'))),
            array(true, array('date' => array('end' => '2216-01-01 12:13:00'))),
            array(true, array('date' => array('start' => '2016-01-01 12:13:00','end' => '2216-01-01 12:13:00'))),
            array(false, array('date' => array('start' => '2016-01-01 12:13:00','end' => '2016-01-01 12:30:00'))),
        );
    }

    /**
     * @dataProvider provideDate
     * @param boolean $expected
     * @param string $flagValue
     */
    public function testCheckDate($expected, $flagValue)
    {
        $container = new Container();
        $container->setParameter(ConfigFileHandler::FEATURE_FLAGS_PARAMETER_NAME, array('flags' => $this->flags));

        $configFileHandler = new ConfigFileHandler($container);
        $classRefl = new \ReflectionClass($configFileHandler);
        $methodRefl = $classRefl->getMethod('checkDate');
        $methodRefl->setAccessible(true);

        self::assertEquals($expected, $methodRefl->invoke($configFileHandler, $flagValue));
    }

    /**
     * @expectedException \Dknx01\FeatureFlagBundle\Exception\InvalidConfigurationValueException
     * @expectedExceptionMessage The value 2016-01-01 does not match pattern ^\d{4}-\d{2}-\d{2}\ \d{2}:\d{2}:\d{2}$
     */
    public function testCheckDateWithException()
    {
        $container = new Container();
        $container->setParameter(ConfigFileHandler::FEATURE_FLAGS_PARAMETER_NAME, array('flags' => $this->flags));

        $configFileHandler = new ConfigFileHandler($container);
        $classRefl = new \ReflectionClass($configFileHandler);
        $methodRefl = $classRefl->getMethod('checkDate');
        $methodRefl->setAccessible(true);
        $methodRefl->invoke($configFileHandler, array('date' => array('start' => '2016-01-01')));
    }

    /**
     * @expectedException \Dknx01\FeatureFlagBundle\Exception\InvalidConfigurationValueException
     */
    public function testCheckTimeWithException()
    {
        $now = new \DateTime();

        $container = new Container();
        $container->setParameter(ConfigFileHandler::FEATURE_FLAGS_PARAMETER_NAME, array('flags' => $this->flags));

        $configFileHandler = new ConfigFileHandler($container);
        $classRefl = new \ReflectionClass($configFileHandler);
        $methodRefl = $classRefl->getMethod('checkTime');
        $methodRefl->setAccessible(true);
        $methodRefl->invoke($configFileHandler, array('time' => array('start' => $now->format('H:i'))));
    }

    /**
     * @return array
     */
    public function provideTime()
    {
        $now = new \DateTime();

        $minBefore5 = clone $now;
        $minBefore5->modify('-5 min');

        $minAfter5 = clone $now;
        $minAfter5->modify('+5 min');

        return array(
            array(true, array('time' => array('start' => $minBefore5->format('H:i:s')))),
            array(true, array('time' => array('start' => $now->format('H:i:s')))),
            array(false, array('time' => array('start' => $minAfter5->format('H:i:s')))),
            array(true, array('time' => array(
                'start' => $minBefore5->format('H:i:s'),
                'end' => $minAfter5->format('H:i:s'))
            )),
        );
    }

    /**
     * @dataProvider provideTime
     * @param boolean $expected
     * @param string $flagValue
     */
    public function testCheckTime($expected, $flagValue)
    {
        $container = new Container();
        $container->setParameter(ConfigFileHandler::FEATURE_FLAGS_PARAMETER_NAME, array('flags' => $this->flags));

        $configFileHandler = new ConfigFileHandler($container);
        $classRefl = new \ReflectionClass($configFileHandler);
        $methodRefl = $classRefl->getMethod('checkTime');
        $methodRefl->setAccessible(true);

        self::assertEquals($expected, $methodRefl->invoke($configFileHandler, $flagValue));
    }
}
