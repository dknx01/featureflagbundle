<?php

namespace Dknx01\FeatureFlagBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $featureFlag = $this->get('feature_flag.flag');
        return $this->render(
            'FeatureFlagBundle:Default:index.html.twig',
            array('flags' => $featureFlag->getAllFlags(), 'handler' => $featureFlag->getHandlerClass())
        );
    }
}
