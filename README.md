# FeatureFlagBundle #


This Symfony Bundle provides a feature flag service.
# Installation #
```
#!bash
composer require dknx01/feature-flag-bundle
```
**Configure the parameters.yml**
```
#!yml
dknx01_feature_flags:
        handler: ~
        flags: {  }
```
**Updating routing.yml if you want the GUI**
```
#!yml
imports:
    resource: "@FeatureFlagBundle/Resources/config/routing.yml
```
# Requirements #
* Symfony 2.8.*
* [Doctrine 2.4.*] (if you want to use a database for storing the flags configuration)
# Configuration #
***YAML File***
```
#!yml

dknx01_feature_flags:
    handler: ~
    flags:
        foo:
            ip: ~
            date: ~
            time:
              start: '15:00:00'
            environment: ~
            sapi: ['cli']
            sapi_negation: true
        bar:
            ip: 127.0.0.1
            date:
              startDate: ~
              endDate: ~
```
# Dependencies #
* symfony/symfony: ^2.8
* doctrine/orm: ~2.5
* doctrine/doctrine-bundle: ~1.4
* monolog/monolog: ~1.11

# Database configuration #
Nothing special, just the normal Doctrine configuration [Symfony Databases and Doctrine](http://symfony.com/doc/2.8/doctrine.html).

It will create it own table if the method "getAllFlags" is used (e.g. calls the GUI) and the database handler is used.
# GUI #
A very simple GUI is available at the following route:

feature_flag_homepage: /_featureflags

Example: http://YOUR.DOMAIN/_featureflags
# Usage #
```
#!php
$this->getContainer()->get('feature_flag.flag')->isActive('bar');
```
# Wiki #
For more see the [Wiki](https://bitbucket.org/dknx01/featureflagbundle/wiki/)