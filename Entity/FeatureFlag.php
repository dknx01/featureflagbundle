<?php
/**
 *
 * @author dknx01 <e.witthauer@gmail.com>
 * @since 10.07.16 21:46
 * @package
 *
 */

namespace Dknx01\FeatureFlagBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="FeatureFlag")
 */
class FeatureFlag
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="integer", unique=true)
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true, nullable=false, unique=true)
     * @var string
     */
    private $name;

    /**
     * @ORM\Column(type="json_array", nullable=true)
     * @var array
     */
    private $ip;

    /**
     * @ORM\Column(name="dateStart", type="date", nullable=true)
     * @var \DateTime
     */
    private $dateStart;
    
    /**
     * @ORM\Column(name="dateEnd", type="date", nullable=true)
     * @var \DateTime
     */
    private $dateEnd;

    /**
     * @ORM\Column(name="timeStart", type="string", length=8, nullable=true)
     * @var string
     */
    private $timeStart;

    /**
     * @ORM\Column(name="timeEnd", type="string", length=8, nullable=true)
     * @var string
     */
    private $timeEnd;

    /**
     * @ORM\Column(type="json_array", nullable=true)
     * @var array
     */
    private $environment;

    /**
     * @ORM\Column(type="json_array", nullable=true)
     * @var array
     */
    private $sapi;

    /**
     * @ORM\Column(name="sapiNegation", type="boolean", nullable=true, options={"default": false})
     * @var bool
     */
    private $sapiNegation;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return FeatureFlag
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return FeatureFlag
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return array
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * @param array $ip
     *
     * @return FeatureFlag
     */
    public function setIp(array $ip)
    {
        $this->ip = $ip;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getDateStart()
    {
        return $this->dateStart;
    }

    /**
     * @param \DateTime $dateStart
     *
     * @return FeatureFlag
     */
    public function setDateStart(\DateTime $dateStart)
    {
        $this->dateStart = $dateStart;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getDateEnd()
    {
        return $this->dateEnd;
    }

    /**
     * @param \DateTime $dateEnd
     *
     * @return FeatureFlag
     */
    public function setDateEnd(\DateTime $dateEnd)
    {
        $this->dateEnd = $dateEnd;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTimeStart()
    {
        return $this->timeStart;
    }

    /**
     * @param string $timeStart
     *
     * @return FeatureFlag
     */
    public function setTimeStart($timeStart)
    {
        $this->timeStart = $timeStart;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTimeEnd()
    {
        return $this->timeEnd;
    }

    /**
     * @param string $timeEnd
     *
     * @return FeatureFlag
     */
    public function setTimeEnd($timeEnd)
    {
        $this->timeEnd = $timeEnd;
        return $this;
    }

    /**
     * @return array
     */
    public function getEnvironment()
    {
        return $this->environment;
    }

    /**
     * @param array $environment
     *
     * @return FeatureFlag
     */
    public function setEnvironment(array $environment)
    {
        $this->environment = $environment;
        return $this;
    }

    /**
     * @return array
     */
    public function getSapi()
    {
        return $this->sapi;
    }

    /**
     * @param array $sapi
     *
     * @return FeatureFlag
     */
    public function setSapi(array $sapi)
    {
        $this->sapi = $sapi;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isSapiNegation()
    {
        return $this->sapiNegation;
    }

    /**
     * @param boolean $sapiNegation
     *
     * @return FeatureFlag
     */
    public function setSapiNegation($sapiNegation)
    {
        $this->sapiNegation = $sapiNegation;
        return $this;
    }
}