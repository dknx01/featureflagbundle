<?php
/**
 *
 * @author dknx01 <e.witthauer@gmail.com>
 * @since 09.07.16 19:56
 * @package
 *
 */

namespace Dknx01\FeatureFlagBundle\Entity;

use ArrayAccess;
use ArrayIterator;
use Closure;
use Countable;
use IteratorAggregate;

class FlagCollection implements Countable, IteratorAggregate, ArrayAccess
{
    /**
     * @var array
     */
    private $flags = array();

    /**
     * FlagCollection constructor.
     * @param array $flags
     */
    public function __construct(array $flags = array())
    {
        $this->flags = $flags;
    }


    /**
     * Applies the given function to each element in the collection and returns
     * a new collection with the elements returned by the function.
     *
     * @param Closure $func
     *
     * @return FlagCollection
     */
    public function map(Closure $func)
    {
        return new static(array_map($func, $this->flags));
    }

    /**
     * Returns all the elements of this collection that satisfy the predicate p.
     * The order of the elements is preserved.
     *
     * @param Closure $p The predicate used for filtering.
     *
     * @return FlagCollection A collection with the results of the filter operation.
     */
    public function filter(Closure $p)
    {
        return new static(array_filter($this->flags, $p));
    }

    /**
     * @inheritdoc
     */
    public function count()
    {
        return count($this->flags);
    }

    /**
     * @inheritdoc
     */
    public function getIterator()
    {
        return new ArrayIterator($this->flags);
    }

    /**
     * @inheritdoc
     */
    public function offsetExists($offset)
    {
        return $this->containsKey($offset);
    }

    /**
     * @inheritdoc
     */
    public function offsetGet($offset)
    {
        return $this->get($offset);
    }

    /**
     * @inheritdoc
     */
    public function offsetSet($offset, $value)
    {
        if (!isset($offset)) {
            return $this->add($value);
        }

        $this->set($offset, $value);
    }

    /**
     * @inheritdoc
     */
    public function offsetUnset($offset)
    {
        return $this->remove($offset);
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return serialize($this->toArray());
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return $this->flags;
    }

    /**
     * Checks whether the collection contains an element with the specified key/index.
     *
     * @param string|integer $offset The key/index to check for.
     *
     * @return boolean TRUE if the collection contains an element with the specified key/index,
     *                 FALSE otherwise.
     */
    private function containsKey($offset)
    {
        return isset($this->flags[$offset]) || array_key_exists($offset, $this->flags);
    }

    /**
     * Gets the element at the specified key/index.
     *
     * @param string|integer $key The key/index of the element to retrieve.
     *
     * @return mixed
     */
    private function get($key)
    {
        return isset($this->flags[$key]) ? $this->flags[$key] : null;
    }

    /**
     * Adds an element at the end of the collection.
     *
     * @param mixed $value The element to add.
     *
     * @return boolean Always TRUE.
     */
    private function add($value)
    {
        $this->flags[] = $value;

        return true;
    }

    /**
     * Sets an element in the collection at the specified key/index.
     *
     * @param string|integer $offset   The key/index of the element to set.
     * @param mixed          $value The element to set.
     *
     */
    private function set($offset, $value)
    {
        $this->flags[$offset] = $value;
    }

    /**
     * Removes the element at the specified index from the collection.
     *
     * @param string|integer $offset The kex/index of the element to remove.
     *
     * @return mixed The removed element or NULL, if the collection did not contain the element.
     */
    private function remove($offset)
    {
        if ( ! isset($this->flags[$offset]) && ! array_key_exists($offset, $this->flags)) {
            return null;
        }

        $removed = $this->flags[$offset];
        unset($this->flags[$offset]);

        return $removed;
    }
}