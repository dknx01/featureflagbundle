<?php
/**
 *
 * @author dknx01 <e.witthauer@gmail.com>
 * @since 08.07.16 18:26
 * @package
 *
 */

namespace Dknx01\FeatureFlagBundle\Service;

use Dknx01\FeatureFlagBundle\Entity\FlagCollection;
use Dknx01\FeatureFlagBundle\Exception\NoSuchConfigurationKeyException;
use Dknx01\FeatureFlagBundle\Handler\HandlerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @inheritdoc
 */
class FeatureFlag implements ContainerAwareInterface
{
    /**
     * @var HandlerInterface
     */
    private $handler;

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param array $config
     */
    public function initialize(array $config)
    {
        $this->logger = $this->container->get('logger');
        $this->createHandler($config);
    }

    /**
     * @inheritdoc
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function isActive($flag)
    {
        return $this->handler->isActive($flag);
    }

    /**
     * @return FlagCollection
     */
    public function getAllFlags()
    {
        return $this->handler->getAllFlags();
    }

    /**
     * @return string
     */
    public function getHandlerClass()
    {
        return get_class($this->handler);
    }

    /**
     * @param array $config
     * @throws NoSuchConfigurationKeyException
     * @throws \InvalidArgumentException
     */
    private function createHandler(array $config)
    {
        $handler = $this->getConfigValueFromArray($config, 'handler');
        if (is_null($handler)) {
            $this->logger->debug(
                'FeatureFlag: No handler set, using default handler "ConfigYamlFileHandler"',
                array('serviceName' => 'feature_flag.handler.config_file')
            );
            $this->handler = $this->container->get('feature_flag.handler.config_file');
        } else {
            if (class_exists($handler)) {
                $this->logger->debug(
                    'FeatureFlag: Load handler form class',
                    array('className' => $handler)
                );
                $this->handler = new $handler();
                if (!$this->handler instanceof HandlerInterface) {
                    throw new \InvalidArgumentException(
                        get_class($this->handler) . ' does not implement FeatureFlagBundle\Handler\HandlerInterface.'
                    );
                }
                if ($this->handler instanceof ContainerAwareInterface) {
                    $this->handler->setContainer($this->container);
                }
            } else {
                $this->logger->debug(
                    'FeatureFlag: Handler is not a class, try as service name',
                    array('serviceName' => $handler)
                );
                if ($this->container->has(trim($handler))) {
                    $this->handler = $this->container->get(trim($handler));
                } else {
                    throw new \InvalidArgumentException($handler . ' was not found.');
                }
            }
        }
    }

    /**
     * @param array $config
     * @param string $key
     * @return mixed
     * @throws NoSuchConfigurationKeyException
     */
    private function getConfigValueFromArray(array $config, $key)
    {
        if (array_key_exists($key, $config)) {
            return $config[$key];
        } else {
            throw new NoSuchConfigurationKeyException($key);
        }
    }
}