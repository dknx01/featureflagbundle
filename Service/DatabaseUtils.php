<?php
/**
 *
 * @author dknx01 <e.witthauer@gmail.com>
 * @since 10.07.16 21:30
 * @package
 *
 */

namespace Dknx01\FeatureFlagBundle\Service;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Schema\Column;
use Doctrine\DBAL\Schema\Table;
use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\Mapping\ClassMetadataInfo;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @codeCoverageIgnore
 */
class DatabaseUtils
{
    const FEATURE_FLAG_TABLE_NAME = 'FeatureFlag';
    /**
     * @var ContainerInterface
     */
    protected $container;

    protected function createTableIfNotExists()
    {
        /** @var Connection $conn */
        $conn = $this->container->get('database_connection');
        if (!$conn->getSchemaManager()->tablesExist(self::FEATURE_FLAG_TABLE_NAME)) {
            $indexes = array();
            $table = new Table(
                'FeatureFlag',
                $this->getTableColumns(),
                $indexes,
                array(),
                ClassMetadataInfo::GENERATOR_TYPE_AUTO
            );
            $table->setPrimaryKey(array($table->getColumn('id')->getName()));
            $table->addUniqueIndex(array($table->getColumn('name')->getName()));
            $conn->getSchemaManager()->createTable($table);
        }
    }

    /**
     * @return Column[]
     * @throws DBALException
     */
    private function getTableColumns()
    {
        $id = new Column('id', Type::getType(Type::INTEGER), array('Autoincrement' => true));
        $name = new Column('name', Type::getType(Type::STRING));
        $name->setLength(255)
            ->setNotnull(true);
        $ip = new Column('ip', Type::getType(Type::JSON_ARRAY));
        $ip->setNotnull(false);
        $dateStart = new Column('dateStart', Type::getType(Type::DATE));
        $dateStart->setNotnull(false);
        $dateEnd = new Column('dateEnd', Type::getType(Type::DATE));
        $dateEnd->setNotnull(false);
        $timeStart = new Column('timeStart', Type::getType(Type::STRING));
        $timeStart->setLength(8)
            ->setNotnull(false);
        $timeEnd = new Column('timeEnd', Type::getType(Type::STRING));
        $timeEnd->setLength(8)
            ->setNotnull(false);
        $environment = new Column('environment', Type::getType(Type::JSON_ARRAY));
        $environment->setNotnull(false);
        $sapi = new Column('sapi', Type::getType(Type::JSON_ARRAY));
        $sapi->setNotnull(false);
        $sapiNegation = new Column('sapiNegation', Type::getType(Type::BOOLEAN));
        $sapiNegation->setNotnull(false)
            ->setDefault(false);
        return array($id, $name, $ip, $dateStart, $dateEnd, $timeStart, $timeEnd, $environment, $sapi, $sapiNegation);
    }
}