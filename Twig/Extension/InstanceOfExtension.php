<?php
/**
 *
 * @author dknx01 <e.witthauer@gmail.com>
 * @since 10.07.16 21:30
 * @package
 *
 */

namespace Dknx01\FeatureFlagBundle\Twig\Extension;

class InstanceOfExtension extends \Twig_Extension
{
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('instanceof', array($this, 'instanceOfClass'), array('is_safe_callback' => true)),
        );
    }

    /**
     * @param object|array $object
     * @param string $className
     * @return bool
     */
    public function instanceOfClass($object, $className)
    {
        $className = trim($className);
        if (strtolower($className) === 'array') {
            return is_array($object);
        }
        return $object instanceof $className;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'instance_of';
    }
}
